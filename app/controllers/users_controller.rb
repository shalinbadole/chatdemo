class UsersController < ApplicationController
	def index

	end

	def new
		@user = User.new
	end #new
	
	def create
		@user = User.new(user_params)
 		if @user.save
 			render 'login'
		else
			render 'new'
		end
	end #create
 
 	def show
	 	#byebug
 	   @user = User.find(params[:email_id])
  end #show
	
	def login
		#@user = User.find(params[:email_id, :password])
		#session[:current_user_id] = @user.id
	end  #login
	
	def loggedin
		@user = User.find(params[:email_id, :password])
		if user
			render 'show'
		else
			render 'login'	
		end	
		#session[:current_user_id] = @user.email_id
  	
	end

	def update
		render 'show'
	end

	def logout
		session[:current_user_id] = nil
		render 'login'
	end
	
	private 
		def user_params
			params.require(:user).permit(:email_id, :password, :first_name, :last_name, :dob, :city, :country)
		end #user_params
end # class
