Rails.application.routes.draw do
	
	get 'welcome/index'
	get 'users/login' 
	post 'users/loggedin'
	resources :users
	
	root 'welcome#index'
end
